﻿using Quest.Model;

namespace Quest.Repository
{
	interface ITeamRepository : IRepository< Team >
	{
	}
}
