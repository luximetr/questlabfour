﻿using Quest.Model;

namespace Quest.Repository
{
	interface IPointRepository : IRepository< Point >
	{
	}
}
