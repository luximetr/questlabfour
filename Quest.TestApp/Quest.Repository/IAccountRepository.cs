﻿using Quest.Model;

namespace Quest.Repository
{
	public interface IAccountRepository : IRepository<Account>
	{
	}
}
