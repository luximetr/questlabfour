﻿using Quest.Model;

namespace Quest.Repository
{
	interface IOrderRepository : IRepository< Order >
	{
	}
}
