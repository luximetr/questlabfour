﻿using Quest.Model;

namespace Quest.Repository
{
	interface IScriptRepository : IRepository< Script >
	{
	}
}
