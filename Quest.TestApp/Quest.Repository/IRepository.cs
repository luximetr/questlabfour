﻿using System;
using System.Linq;

using Quest.Utils;

namespace Quest.Repository
{
	public interface IRepository<T> where T : Utils.Entity
	{
		int Count();

		T Load(int id);

		IQueryable<T> LoadAll();

		void Add(T t);

		void Delete(T t);

		void Commit();
	}
}
