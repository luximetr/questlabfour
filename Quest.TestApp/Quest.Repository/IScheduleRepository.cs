﻿using Quest.Model;

namespace Quest.Repository
{
	interface IScheduleRepository : IRepository< Schedule >
	{
	}
}
