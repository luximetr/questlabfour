﻿using Quest.Model;

namespace Quest.Repository
{
	interface ICardRepository : IRepository< Card >
	{
	}
}
