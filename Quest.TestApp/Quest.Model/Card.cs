﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class Card : Utils.Entity
	{
		public string OwnerName { set; get; }
		public string CardNumber { set; get; }
		public string BankName { set; get; }

		public Card(Guid domainId, string ownerName, string cardNumber, string bankName)
			: base(domainId)
		{
			OwnerName = ownerName;
			CardNumber = cardNumber;
			BankName = bankName;
		}

		public override string ToString()
		{
			return String.Format("{0} {1} {2}", CardNumber, OwnerName, BankName);
		}
	}
}
