﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model
{
	public class AdministratorAccount : Account
	{
		public AdministratorAccount(Guid domainId, string login, string password)
		: base(domainId, login, password)
		{
		}

		public override string ToString()
		{
			return String.Format("login: {0}, password: {1}", Login, Password);
		}
	}
}
