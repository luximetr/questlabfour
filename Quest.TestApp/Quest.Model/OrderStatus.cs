﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quest.Model.Enums
{
	public enum OrderStatus
	{
		Placed,
		Confirmed,
		Canceled
	}
}
